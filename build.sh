#!/bin/sh

SCRIPT_DIR=$(readlink -f $(dirname $0))
. "${SCRIPT_DIR}/functions.sh"

if [ "x${NDK}" != "x" ]; then
    check_path ${NDK}
else
    error "Specify path to android ndk as NDK"
fi

if [ "x${ABI}" != "x" ]; then
    check_abi ${ABI}
else
    ABI="all"
fi

info "NDK in ${NDK}"
info "Building for ${ABI}"

BUILD_ABI=${ABI} NDK_PROJECT_PATH=${SCRIPT_DIR} ${NDK}/ndk-build V=1
