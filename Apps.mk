# Copyright 2006 The Android Open Source Project

LOCAL_PATH:= $(call my-dir)

local_c_includes :=
local_c_flags :=

local_additional_dependencies := $(LOCAL_PATH)/android-config.mk $(LOCAL_PATH)/Apps.mk

include $(LOCAL_PATH)/Apps-config.mk

ifneq ($(ENABLE_STATIC),true)

    include $(CLEAR_VARS)
    LOCAL_MODULE:= openssl
    LOCAL_CLANG := true
    LOCAL_MODULE_TAGS := optional
    LOCAL_LDLIBS := -lz
    LOCAL_SRC_FILES := $(target_src_files)
    LOCAL_SHARED_LIBRARIES := libssl libcrypto
    LOCAL_C_INCLUDES := $(target_c_includes)
    LOCAL_CFLAGS := $(target_c_flags)
    LOCAL_ADDITIONAL_DEPENDENCIES := $(local_additional_dependencies)
    include $(LOCAL_PATH)/android-config.mk
    include $(BUILD_EXECUTABLE)

endif
