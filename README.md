openssl-AndroidBuild
--------------------

This is the fork of official Android openssl sources:  
https://github.com/android/platform_external_openssl.git

Only for linux hosts.

Building:

`cd openssl-androidbuild`

`NDK=/path/to/android/ndk sh build.sh`

to build for specific ABI (default ABI=all) specify:

`ABI=armeabi-v7`

For static build use:

`ENABLE_STATIC=true`